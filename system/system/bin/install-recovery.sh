#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:d16881e9b8b965a4fd0873cdb3377ee6a162e1ca; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:0645e68fe3894d8cbb16f19ae4a08e7513bfc1ec \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:d16881e9b8b965a4fd0873cdb3377ee6a162e1ca && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
