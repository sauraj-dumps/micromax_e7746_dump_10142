## full_k69v1_64-user 10 QP1A.190711.020 1620836268 release-keys
- Manufacturer: micromax
- Platform: mt6768
- Codename: E7746
- Brand: Micromax
- Flavor: full_k69v1_64-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 1620836268
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Micromax/E7746/E7746:10/QP1A.190711.020/1620836268:user/release-keys
- OTA version: 
- Branch: full_k69v1_64-user-10-QP1A.190711.020-1620836268-release-keys
- Repo: micromax_e7746_dump_10142


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
